<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ScoresRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      attributes={
 *          "order"={"playedAt": "DESC"}
 *      },
 *      paginationItemsPerPage=3,
 *      normalizationContext={"groups"={"read:score"}},
 *      denormalizationContext={"groups"={"write:score"}}
 * )
 * @ORM\Entity(repositoryClass=ScoresRepository::class)
 */
class Scores
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * 
     * @Groups("read:score")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * 
     * @Groups("read:score")
     */
    private $playedAt;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read:score", "write:score"})
     */
    private $score;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"read:score", "write:score"})
     */
    private $nbrOfCorrectAnsswers;

    /**
     * @ORM\Column(type="integer")
     * 
     * @Groups({"read:score", "write:score"})
     */
    private $nbrOfWrongAnswers;


    public function __construct() {
        $this->playedAt = new DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayedAt(): ?\DateTimeInterface
    {
        return $this->playedAt;
    }

    public function setPlayedAt(\DateTimeInterface $playedAt): self
    {
        $this->playedAt = $playedAt;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNbrOfCorrectAnsswers(): ?int
    {
        return $this->nbrOfCorrectAnsswers;
    }

    public function setNbrOfCorrectAnsswers(int $nbrOfCorrectAnsswers): self
    {
        $this->nbrOfCorrectAnsswers = $nbrOfCorrectAnsswers;

        return $this;
    }

    public function getNbrOfWrongAnswers(): ?int
    {
        return $this->nbrOfWrongAnswers;
    }

    public function setNbrOfWrongAnswers(int $nbrOfWrongAnswers): self
    {
        $this->nbrOfWrongAnswers = $nbrOfWrongAnswers;

        return $this;
    }
}
